const express = require("express");
const bodyParser = require("body-parser");
require("dotenv").config();
const app = express();
const port = 3000;
const userDetails = require("./userDetails.json");
const viewRoutes = require("./routes/viewRoutes");
const apiRoutes = require("./routes/apiRoutes");
const authRoutes = require("./routes/auth");
const mongoose = require("mongoose");
const { LibManifestPlugin } = require("webpack");
var session = require("express-session");
const MongoStore = require("connect-mongo");
const passport = require("passport");

if (process.env.MODE == "production") {
    app.use(
        session({
            secret: "keyboard cat",
            resave: false,
            saveUninitialized: false,
            store: MongoStore.create({
                mongoUrl: `mongodb://felipe:${process.env.MONGO_PASSWORD}@192.168.171.67:27017/felipe?authSource=admin`,
            }),
        })
    );
} else {
    app.use(
        session({
            secret: "keyboard cat",
            resave: false,
            saveUninitialized: false,
            store: MongoStore.create({ mongoUrl: `mongodb://127.0.0.1/goose` }),
        })
    );
}

app.use(passport.initialize());
app.use(passport.session());

const aLoggerMiddleware = (req, res, next) => {
    console.log(req.method, req.url);
    next();
};
app.use(aLoggerMiddleware);
app.use(
    bodyParser.urlencoded({ extended: false }),
    bodyParser.json({ extended: false })
);
app.use(express.static("public"), express.static("dist"));
app.use(viewRoutes);
app.use("/api", apiRoutes);
app.use(authRoutes);

async function main() {
    if (process.env.MODE == "production") {
        await mongoose.connect(`mongodb://192.168.171.67:27017/nic`, {
            useNewUrlParser: true,
            authSource: "admin",
            user: "nic",
            pass: process.env.MONGO_PASSWORD,
        });
    } else {
        await mongoose.connect("mongodb://localhost:27017/goose");
    }
    app.listen(port, () => {
        console.log(`Example app listening on port ${port}`);
    });
}

main().catch((err) => console.error(err));
