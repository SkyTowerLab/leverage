const model = {
  finnhubToken: "cc3nfp2ad3i9vsk3u7gg",
  isSearchingCryptos: false,
  userData: JSON.parse(localStorage.getItem("userData")) || {
    assets: {
      GOOG: { symbol: "GOOG", shares: 1, type: "stock", price: 100 },
      AAPL: { symbol: "AAPL", shares: 0, type: "stock", price: 100 },
      AMZN: { symbol: "AMZN", shares: 0, type: "stock", price: 100 },
      BTCUSDT: { symbol: "BTCUSDT", shares: 0, type: "crypto", price: 100 },
    },
    info: {
      balance: 10000,
    },
  },
  timerInterval: null,
  timeLeft: 15,
  setTimeLeft(time) {
    model.timeLeft = time;
    view.updateTimer();
  },
  focusedAsset: null,
  setFocusedAsset(asset) {
    this.focusedAsset = asset;
    view.updateFocus(asset);
  },
  updatePrice(symbol, price) {
    this.userData.assets[symbol].price = price;
    view.refreshAssetList();
    controller.setStorage();
  },
  updateBalance(newBalance) {
    this.userData.info.balance = newBalance;
    controller.setStorage();
  },
  updateShares(symbol, shares) {
    this.userData.assets[symbol].shares = shares;
    view.refreshPage(this.focusedAsset);
    controller.setStorage();
  },
};

const view = {
  //using method shorthand: `refreshPage(stockInFocus) {` == `refreshPage: function(stockInFocus) {`
  refreshPage(stockInFocus) {
    controller.focusTicker(stockInFocus);
    controller.setStorage();
    view.refreshAssetList();
  },
  updateElementAssetPrice(element, price) {
    element.innerText = view.displayDollars(price);
  },
  updateElementAssetValuation(element, price, numShares, isPrefixed = true) {
    const prefix = isPrefixed ? `${numShares} shares:` : "";
    const displayText = prefix + view.displayDollars(price * numShares);
    element.innerText = displayText;
  },
  refreshAssetList() {
    view.removeChildren(stockList);
    for (asset in model.userData.assets) {
      view.createAssetListItem(model.userData.assets[asset]);
    }
    view.intializeMaterialList();
  },
  displayDollars(number) {
    return `$${number.toFixed(2)}`;
  },
  removeChildren(domNode) {
    while (domNode.firstChild) {
      domNode.removeChild(domNode.firstChild);
    }
  },
  makeMaterialButton(buttonAction) {
    const newButton = document.createElement("button");
    newButton.classList.add(
      "mdc-button",
      "mdc-card__action",
      "mdc-card__action--button"
    );

    const rippleDiv = document.createElement("div");
    rippleDiv.classList.add("mdc-button__ripple");
    newButton.appendChild(rippleDiv);

    const labelSpan = document.createElement("span");
    labelSpan.classList.add("mdc-button__label");
    labelSpan.innerText = buttonAction;
    newButton.appendChild(labelSpan);

    const MDCRipple = mdc.ripple.MDCRipple;
    const theNewRipple = new MDCRipple(newButton);

    return newButton;
  },
  displaySearchError(errorMessage) {
    searchHelperText.innerText = errorMessage;
    searchHelperText.classList.add(
      "mdc-text-field-helper-text--validation-msg"
    );
    searchHelperText.classList.remove("mdc-text-field-helper-text");
  },
  hideSearchError() {
    searchHelperText.innerText = "";
    searchHelperText.classList.remove(
      "mdc-text-field-helper-text--validation-msg"
    );
    searchHelperText.classList.add("mdc-text-field-helper-text");
  },
  createAssetListItem(asset) {
    const newListItem = document.createElement("li");
    newListItem.classList.add("mdc-list-item", "space-between", "flex-center");
    newListItem.setAttribute("role", "option");
    newListItem.setAttribute("tabindex", "0");
    newListItem.setAttribute("data-ticker", asset.symbol);

    const rippleSpan = document.createElement("span");
    rippleSpan.classList.add("mdc-list-item__ripple");
    newListItem.appendChild(rippleSpan);

    const textSpan = document.createElement("span");
    textSpan.classList.add("mdc-list-item__text");
    textSpan.innerText = asset.symbol;
    newListItem.appendChild(textSpan);
    const usersShares = asset.shares;
    const sharesSpan = document.createElement("span");
    if (usersShares) {
      sharesSpan.innerText = "Loading ... ";
      view.updateElementAssetValuation(sharesSpan, asset.price, usersShares);
    } else {
      sharesSpan.innerText = `just watching`;
    }
    newListItem.appendChild(sharesSpan);
    const priceSpan = document.createElement("span");
    priceSpan.innerText = " Loading ...";
    newListItem.appendChild(priceSpan);

    const spacer = document.createElement("span");
    newListItem.appendChild(spacer);

    view.updateElementAssetPrice(priceSpan, asset.price);
    stockList.appendChild(newListItem);
  },
  async initializePage() {
    controller.focusTicker(Object.values(model.userData.assets)[0]);
    view.refreshAssetList();
    view.initializeHelperText();
    try {
      const fetchedBalanceResponse = await fetch("/api/userBalance");
      const fetchedBalanceData = await fetchedBalanceResponse.json();
      model.updateBalance(fetchedBalanceData.balance);
    } catch (e) {
      console.error("error fetching balance", e);
    }
  },
  intializeMaterialList() {
    const MDCList = mdc.list.MDCList;
    const MDCRipple = mdc.ripple.MDCRipple;
    const list = new MDCList(document.querySelector(".mdc-list"));
    const listItemRipples = list.listElements.map((listItemEl) => {
      return new MDCRipple(listItemEl);
    });
  },
  initializeHelperText() {
    const MDCTextFieldHelperText = mdc.textField.MDCTextFieldHelperText;
    const helperText = new MDCTextFieldHelperText(
      document.querySelector(".mdc-text-field-helper-text")
    );
  },
  updateTimer() {
    timerSpan.innerText = model.timeLeft;
  },
  alertUser(message) {
    alertDialog.open();
    alertContent.innerText = message;
  },
  dismissAlert() {
    alertDialog.close();
  },
  updateFocus(asset) {
    view.removeChildren(focusedDescription);
    view.removeChildren(focusedCTAs);

    const stockHeadlines = document.createElement("p");
    stockHeadlines.classList.add(
      "flex-center",
      "space-between",
      "ampler-padding",
      "half-width"
    );
    const stockSymbol = document.createElement("span");
    stockSymbol.innerText = asset.symbol;
    const stockPrice = document.createElement("span");
    view.updateElementAssetPrice(stockPrice, asset.price);
    stockHeadlines.append(stockSymbol, stockPrice);
    focusedDescription.appendChild(stockHeadlines);

    const usersAsset = model.userData.assets[asset.symbol];
    const usersShares = usersAsset?.shares;

    const buyButton = view.makeMaterialButton("Buy");
    buyButton.addEventListener("click", function () {
      controller.buyStock(asset);
    });
    focusedCTAs.appendChild(buyButton);

    if (usersShares) {
      const userStake = document.createElement("p");
      userStake.classList.add(
        "flex-center",
        "space-between",
        "ampler-padding",
        "half-width"
      );
      const numberOfShares = document.createElement("span");
      numberOfShares.innerText = `${usersShares} shares`;
      const valuation = document.createElement("span");
      view.updateElementAssetValuation(
        valuation,
        asset.price,
        usersShares,
        false
      );
      userStake.append(numberOfShares, valuation);
      focusedDescription.appendChild(userStake);
      const sellButton = view.makeMaterialButton("Sell");
      sellButton.addEventListener("click", () => {
        controller.sellStock(asset);
      });
      focusedCTAs.appendChild(sellButton);
    }

    if (usersAsset) {
      //this is equivalent to the fact that it is being watched
      const ignoreButton = view.makeMaterialButton("Ignore");
      ignoreButton.addEventListener("click", function () {
        controller.ignoreStock(asset);
      });
      focusedCTAs.appendChild(ignoreButton);
    } else {
      const watchButton = view.makeMaterialButton("Watch");
      watchButton.addEventListener("click", function () {
        controller.watchStock(asset);
      });
      focusedCTAs.appendChild(watchButton);
    }

    const stockDescription = document.createElement("p");
    stockDescription.innerText = `This lorem concerning ${asset.symbol} ipsum dolor sit amet consectetur adipisicing elit. Tempore, at ullam repellendusexpedita aperiam optio, rem quos voluptate ea facere velit cumcommodi placeat nesciunt deserunt quidem. Aspernatur,repellendus nobis?`;
    focusedDescription.appendChild(stockDescription);

    tickerSearchInput.setAttribute("value", asset.symbol);
  },
};

const controller = {
  buyStock(stock) {
    model.tradeMode = "buy";
    model.focusedAsset = stock;
    tradeTitle.innerText = `Buy ${stock.symbol}`;
    tradeDialog.open();
    controller.startTimer();
  },
  sellStock(stock) {
    model.userData.assets[stock.symbol].shares -= 1;
    view.refreshPage(stock);
  },
  watchStock(stock) {
    model.userData.assets[stock.symbol].shares = 0;
    view.refreshPage(stock);
  },
  ignoreStock(stock) {
    if (model.userData.assets[stock.symbol].shares) {
      alert(" it is prudent to sell before you ignore! ");
    } else {
      delete model.userData.assets[stock.symbol];
    }
    view.refreshPage(stock);
  },
  confirmTrade() {
    clearInterval(model.timerInterval);
    const tradedAsset = model.focusedAsset;
    const numShares = parseInt(tradeSharesInput.value);
    const userCash = model.userData.info.balance;
    const userShares = model.userData.assets[tradedAsset.symbol]?.shares ?? 0;
    const tradePrice = tradedAsset.price * numShares;
    if (model.tradeMode == "buy") {
      if (tradePrice > userCash) {
        controller.closeTradeDialog();
        view.alertUser("get some money first");
      } else {
        console.log("congratulations, you had the money");
        model.updateBalance(userCash - tradePrice);
        model.updateShares(tradedAsset.symbol, userShares + numShares);
      }
    }
    tradeDialog.close();
  },
  async fetchStockPrice(stockSymbol) {
    try {
      if (!stockSymbol) return;
      const res = await fetch(
        `https://finnhub.io/api/v1/quote?symbol=${stockSymbol}&token=${model.finnhubToken}`
      );
      const data = await res.json();

      if (data.c == 0) throw `Oh no! Probably ${stockSymbol} is not a stock`;
      view.hideSearchError();
      if (Object.keys(model.userData.assets).includes(stockSymbol)) {
        model.updatePrice(stockSymbol, data.c);
      }
      return { price: data.c, symbol: stockSymbol, type: "stock" };
    } catch (thrownError) {
      view.displaySearchError(thrownError);
    }
  },
  async fetchCryptoPrice(crytpoSymbol) {
    try {
      const baseUrl = "https://api.binance.com";
      const res = await fetch(
        baseUrl + `/api/v3/ticker/price?symbol=${crytpoSymbol}`
      );
      const data = await res.json();
      view.hideSearchError();
      const fixedPrice = parseFloat(parseFloat(data.price).toFixed(2));
      if (Object.keys(model.userData.assets).includes(crytpoSymbol)) {
        model.updatePrice(crytpoSymbol, fixedPrice);
      }
      return {
        price: fixedPrice,
        symbol: crytpoSymbol,
        type: "crypto",
      };
    } catch (_) {
      view.displaySearchError(`Sure ${crytpoSymbol} is a crypto?`);
    }
  },
  fetchAssetPrice(asset) {
    return asset.type == "crypto"
      ? controller.fetchCryptoPrice(asset.symbol)
      : controller.fetchStockPrice(asset.symbol);
  },
  toggleSearchMode() {
    focusHeadline.innerText = model.isSearchingCryptos
      ? "Searching Stocks"
      : "Searching Cryptos";
    toggleButton.childNodes[3].innerText = model.isSearchingCryptos
      ? "Search Cryptos"
      : "Search Stocks";
    model.isSearchingCryptos = !model.isSearchingCryptos;
  },
  async searchTicker() {
    const searchedTicker = tickerSearchInput.value;
    if (controller.validateTickerInput(searchedTicker)) {
      const data = await controller.fetchAssetPrice({
        symbol: searchedTicker,
        type: model.isSearchingCryptos ? "crypto" : "stock",
      });
      model.setFocusedAsset(data);
    } else {
      view.displaySearchError("you are searching for nothing");
    }
  },
  validateTickerInput(tickerSymbol) {
    return tickerSymbol !== "";
  },
  async focusTicker(asset) {
    const data = await controller.fetchAssetPrice(asset);
    model.setFocusedAsset(data);
  },
  focusListItem(event) {
    let nodeOfInterest = event.target;
    if (nodeOfInterest.matches("ul")) return;
    while (!nodeOfInterest.matches("li")) {
      nodeOfInterest = nodeOfInterest.parentNode;
    }
    const dataAttribute = nodeOfInterest.dataset["ticker"];
    controller.focusTicker(model.userData.assets[dataAttribute]);
  },
  setStorage() {
    localStorage.setItem("userData", JSON.stringify(model.userData));
  },
  startTimer() {
    model.setTimeLeft(15);
    model.timerInterval = setInterval(() => {
      if (model.timeLeft == 0) {
        return controller.timeoutTrade(model.timerInterval);
      }
      model.setTimeLeft(model.timeLeft - 1);
    }, 1000);
  },
  closeTradeDialog() {
    clearInterval(model.timerInterval);
    tradeDialog.close();
  },
  timeoutTrade() {
    this.closeTradeDialog();
    view.alertUser("Please Confirm Trade");
  },
};

const focusHeadline = document.querySelector("#focus-headline");
const focusedDescription = document.querySelector("#focused-description");
const focusedCTAs = document.querySelector("#focused-ctas");
const toggleButton = document.querySelector("#toggle-focus");
toggleButton.addEventListener("click", controller.toggleSearchMode);

const tickerSearchIcon = document.querySelector("#ticker-search");
const tickerSearchInput = document.querySelector("#ticker-input");
const searchHelperText = document.querySelector("#helper-text");
tickerSearchIcon.addEventListener("click", controller.searchTicker);
tickerSearchInput.addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    event.preventDefault();
    tickerSearchIcon.click();
  }
});
const stockList = document.querySelector("ul#stock-list");
stockList.addEventListener("click", controller.focusListItem);
//lev1
const tradeDialog = new mdc.dialog.MDCDialog(
  document.querySelector("#trade-dialog")
);
const tradeTitle = document.querySelector("#trade-title");
const confirmTradeButton = document.querySelector("#confirm-trade-button");
confirmTradeButton.addEventListener("click", controller.confirmTrade);
const tradeSharesInput = document.querySelector("#trade-shares-input");
const timerSpan = document.querySelector("#trade-timer");
const alertDialog = new mdc.dialog.MDCDialog(
  document.querySelector("#alert-dialog")
);
const alertContent = document.querySelector("#alert-content");
const alertButton = document.querySelector("#alert-button");
alertButton.addEventListener("click", view.dismissAlert);

view.initializePage();
// deal with the socket stuff this time...
let socket;

function openBinanceWebsocket() {
  socket = new WebSocket("wss://stream.binance.com:9443/ws/btcusdt@miniTicker");

  socket.onopen = function (event) {
    console.log("Websocket connetion open", event);
  };

  socket.onmessage = function (event) {
    console.log(event);
    let data = JSON.parse(event.data);
    console.log(data);
    console.log(
      "current bitcoin price",
      view.displayDollars(parseFloat(data.c))
    );
  };
}

function subscribeBinanceSymbol(symbol) {
  socket.send(
    JSON.stringify({
      method: "SUBSCRIBE",
      params: [`${symbol}@miniTicker`],
      id: 1,
    })
  );
}
