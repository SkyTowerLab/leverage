const { Router } = require("express");
const userRouter = Router();
const apiRouter = Router();
const { User, Asset } = require("../models");

userRouter.get("/:id", async (req, res) => {
  try {
    const searchedUser = await User.findById(req.params.id);
    res.json(searchedUser);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

userRouter.post("/", async (req, res) => {
  try {
    const newUser = await User({
      ...req.body,
      balance: 6789,
      assets: [],
    });
    const savedUser = await newUser.save();
    res.json({ id: savedUser._id });
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

apiRouter.use("/user", userRouter);

apiRouter.post("/trade", async (req, res) => {
  try {
    const { numberOfShares, userId, assetType, assetSymbol, tradeType } =
      req.body;
    const numberShares = parseInt(numberOfShares);
    if (tradeType !== "buy") return res.sendStatus(500);
    //stub fetching stock price at first
    const price = 100;
    const user = await User.findById(userId).populate("assets");
    const { balance, assets } = user;

    const tradeTotal = price * numberShares;
    if (tradeTotal > balance) return res.sendStatus(500);

    const foundAsset = assets.find((asset) => asset.symbol == assetSymbol);
    if (foundAsset) {
      foundAsset.shares = foundAsset.shares + numberShares;
      await foundAsset.save();
    } else {
      const newAsset = new Asset({
        symbol: assetSymbol,
        type: assetType,
        shares: numberShares,
      });
      await newAsset.save();
      user.assets.push(newAsset);
    }

    user.balance = user.balance - tradeTotal;
    await user.save();
    res.sendStatus(200);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

module.exports = apiRouter;
