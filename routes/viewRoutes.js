const router = require("express").Router();
const path = require("path");

const serveLandingPage = (req, res) => {
    res.sendFile(path.join(__dirname, "../public/index.html"));
};

const servePortfolioPage = (req, res) => {
    res.sendFile(path.join(__dirname, "../public/portfolio.html"));
};

router.get("/", serveLandingPage);

router.get("/about", serveLandingPage);

router.get("/portfolio", ensureAuthenticated, servePortfolioPage);

router.get("/portfolio.html", ensureAuthenticated, servePortfolioPage);

router.get("/login", (req, res) => {
    res.sendFile(path.join(__dirname, "../public/login.html"));
});

function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect("/login");
}

module.exports = router;
