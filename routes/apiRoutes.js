const { Router } = require("express");
const userRouter = Router();
const apiRouter = Router();
const { User, Asset } = require("../models");
const fetch = require("node-fetch");
const finnhubToken = process.env.FINNHUB_TOKEN;

userRouter.get("/:id", async (req, res) => {
  try {
    const searchedUser = await User.findById(req.params.id).populate("assets");
    res.json(searchedUser);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

userRouter.post("/", async (req, res) => {
  try {
    const newUser = await User({
      ...req.body,
      balance: 6789,
      assets: [],
    });
    const savedUser = await newUser.save();
    res.json({ id: savedUser._id });
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

apiRouter.use("/user", userRouter);

apiRouter.post("/trade", async (req, res) => {
  try {
    const { numberOfShares, userId, assetType, assetSymbol, tradeType } =
      req.body;
    const numberShares = parseInt(numberOfShares);
    if (tradeType !== "buy")
      return res.status(500).send("only buying is supported currently");
    const quoteResponse = await fetch(
      `https://finnhub.io/api/v1/quote?symbol=${assetSymbol}&token=${finnhubToken}`
    );
    const quoteData = await quoteResponse.json();
    const price = quoteData.c;
    const user = await User.findById(userId).populate("assets");
    const { balance, assets } = user;

    const tradeTotal = price * numberShares;
    if (tradeTotal > balance) return res.status(500).send("get more money");

    const foundAsset = assets.find((asset) => asset.symbol == assetSymbol);
    if (foundAsset) {
      foundAsset.shares = foundAsset.shares + numberShares;
      await foundAsset.save();
    } else {
      const newAsset = new Asset({
        symbol: assetSymbol,
        type: assetType,
        shares: numberShares,
      });
      await newAsset.save();
      user.assets.push(newAsset);
    }

    user.balance = user.balance - tradeTotal;
    await user.save();
    res.sendStatus(200);
  } catch (err) {
    console.error(err);
    res.sendStatus(500);
  }
});

module.exports = apiRouter;
