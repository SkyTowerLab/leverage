const mongoose = require("mongoose");
const crypto = require("crypto");

const userSchema = new mongoose.Schema({
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    securityQuestion: { type: String, required: true },
    securityAnswer: { type: String, required: true },
    balance: { type: Number, required: true },
    assets: [{ type: mongoose.Schema.Types.ObjectId, ref: "Asset" }],
});

userSchema.pre("save", function (next) {
    let user = this;
    if (!user.isModified("password") || !user.isNew) return next();

    try {
        crypto.pbkdf2(
            user.password,
            "salt", // 16 random bytes
            310000,
            32,
            "sha256",
            function (err, hashedPassword) {
                if (err) {
                    return next(err);
                }
                user.password = hashedPassword.toString("base64");
                next();
            }
        );
    } catch (err) {
        console.error(err);
    }
});

userSchema.methods.validatePassword = function (triedPassword, cb) {
    let user = this;
    crypto.pbkdf2(
        triedPassword,
        "salt", // 16 random bytes
        310000,
        32,
        "sha256",
        function (err, hashedPassword) {
            if (err) {
                return cb(err);
            }

            const firstBuff = Buffer.from(user.password, "base64");
            const secondBuffer = Buffer.from(hashedPassword, "base64");
            if (
                firstBuff.length != secondBuffer.length ||
                !crypto.timingSafeEqual(firstBuff, secondBuffer)
            ) {
                return cb(null, false, {
                    message: "incorrect username or password",
                });
            }

            return cb(null, user);
        }
    );
};

const User = mongoose.model("User", userSchema);
module.exports = User;
